// Check off specific todos by clicking

$('ul').on('tap click', 'li', function(e) {
  $(this).toggleClass('completed');
  var xhr = new XMLHttpRequest();
  var url = 'https://todo-hua.herokuapp.com/check?_method=PUT';
  xhr.open('POST', url, true);
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.onreadystatechange = function(){
    if(xhr.readyState == 4 && xhr.status == 200){
      console.log(xhr.responseText);
    }
  };

  xhr.send(JSON.stringify(
    {
      id: this.id
    }
  ));

});

//Click on X to delete todo
$('ul').on('click tap', 'span', function (event) {
  var xhr = new XMLHttpRequest();
  var url = 'https://todo-hua.herokuapp.com/delete?_method=DELETE';
  xhr.open('POST', url, true);
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.onreadystatechange = function(){
    if(xhr.readyState == 4 && xhr.status == 200){
      console.log(xhr.responseText);
    }
  };

  xhr.send(JSON.stringify(
    {
      id: this.id
    }
  ));
  $(this).parent().fadeOut(500, function () {
    $(this).remove();
  });
  event.stopPropagation();
});


$('input[type="text"]').keypress(function (event) {
  if(event.which === 13){ // check for the enter keypress
    let todo = $(this).val();
    $(this).val('');
    if(todo == ''){
      return;
    } else {
      $('ul').append('<li><span><i class="fa fa-trash-alt"></i></span> '+todo+'</li>');
      var xhr = new XMLHttpRequest();
      var url = 'https://todo-hua.herokuapp.com/new';
      xhr.open('POST', url, true);
      xhr.setRequestHeader('Content-Type', 'application/json');
      // xhr.withCredentials = true;

      xhr.onreadystatechange = function(){
        if(xhr.readyState == 4 && xhr.status == 200){
          console.log(xhr.responseText);
        }
      };

      xhr.send(JSON.stringify(
        {
          text: todo,
          isChecked: false,
        }
      ));

    }
  }
});

$('.fa-sign-out-alt').click(function () {
  window.location.replace("https://todo-hua.herokuapp.com/logout")
});
