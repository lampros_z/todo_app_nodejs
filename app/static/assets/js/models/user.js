var mongoose = require('mongoose');
var passportLocalMongoose = require('passport-local-mongoose');

var UserSchema = new mongoose.Schema(
	{
		username: String,
		password: String
	}
);

UserSchema.plugin(passportLocalMongoose); //Adds more functionality to our user schema that it is required for our auth

module.exports = mongoose.model("User", UserSchema);
