var mongoose = require('mongoose');


var TodoSchema = new mongoose.Schema(
	{
		text: String,
		isChecked: Boolean,
    date: { type: Date, default:Date.now },
    author: {
        id: {
          type: mongoose.Schema.Types.ObjectId,
          ref: "User"
        },
        username: String
    }
	}
);

module.exports = mongoose.model("Todo", TodoSchema);
