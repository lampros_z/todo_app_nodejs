var express           = require('express'),
mongoose              = require('mongoose'),
passport              = require('passport'),
bodyParser            = require('body-parser'),
localStrategy         = require('passport-local'),
User                  = require(__dirname+'/static/assets/js/models/user.js'),
Todo                  = require(__dirname+'/static/assets/js/models/todo.js'),
methodOverride        = require('method-override'),
session               = require('express-session'),
passportLocalMongoose = require('passport-local-mongoose'),
cors                  = require('cors');



var app = express();

//==================================================
mongoose.connect('mongodb://lampros18:admin@webdevelopment-shard-00-00-x6qac.mongodb.net:27017,webdevelopment-shard-00-01-x6qac.mongodb.net:27017,webdevelopment-shard-00-02-x6qac.mongodb.net:27017/todos?ssl=true&replicaSet=WebDevelopment-shard-0&authSource=admin&retryWrites=true', { useNewUrlParser: true });
//==================================================

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(express.static(__dirname + '/static'));
app.use(cors());
app.set('view engine', 'ejs');
app.use(methodOverride('_method'));
app.set('trust proxy', 1) // trust first proxy
//passport setup here
const MongoStore = require('connect-mongo')(session);
app.use(session({
  secret: 'ANalysis and design with uml and vpP',
  resave: false,
  saveUninitialized: true,
  // cookie: { secure: true },
  store: new MongoStore({ mongooseConnection: mongoose.connection })
}))
app.use(passport.initialize());
app.use(passport.session());

passport.use(new localStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser()); //Reading the session, taking the data from the session and encode them
passport.deserializeUser(User.deserializeUser()); //Reading the session, taking the data from the session and decode them
app.use(function (req, res, next) {
  res.locals.currentUser = req.user;
  next();
});

app.enable('trust proxy');

// Add a handler to inspect the req.secure flag (see
// http://expressjs.com/api#req.secure). This allows us
// to know whether the request was via http or https.
app.use (function (req, res, next) {
        if (req.secure) {
                // request was via https, so do no special handling
                next();
        } else {
                // request was via http, so redirect to https
                res.redirect('https://' + req.headers.host + req.url);
        }
});
// Routes
//Index
app.get('/', isLoggedIn, function(req, res){
  var author = {
    id : req.user._id,
    username: req.user.username
  };
  Todo.find({author:author}, function (err, todos) {
    if(err)
    console.error(err);
    else
    res.render('index.ejs', {todos:todos});
  });
});


//Create todo route
app.post('/new', isLoggedIn ,function (req ,res) {
  var text = req.body.text;
  var isChecked = req.body.isChecked;
  var author = {
    id : req.user._id,
    username: req.user.username
  };
  var newTodo = {text:text, isChecked:isChecked, author:author};
  Todo.create(newTodo, function (err, todo) {
    if(err)
      res.send();
    res.send();
  });
});

// update for the isChecked
app.put('/check', isLoggedIn, function (req, res) {
  Todo.findById(req.body.id, function (err, foundTodo) {
    if(err)
    {
      console.error(err);
    } else {
      if(foundTodo.isChecked){
        foundTodo.isChecked = false;
        foundTodo.save();
      } else{
        foundTodo.isChecked = true;
        foundTodo.save();
      }
    }
  });
    res.send();
});

app.delete('/delete', isLoggedIn, function (req, res) {
  Todo.findByIdAndDelete(req.body.id, function (err) {
     res.send();
  })
});
//Register Routes
app.get('/register', function(req, res){
  res.render('register');
});


app.post('/register', function(req, res){
  User.register(new User({username:req.body.username}), req.body.password, function(err, user){
    if(err)
    {
      console.error(err);
      return res.render('register');
    } else {
      passport.authenticate('local')(req, res, function(){ // It will log the user in the system //local = strategy
        res.redirect('/');
      });
    }
  });
});


//Login routes
//LOGIN LOGIC
//MIDDLEWARE - IT IS AMID THE BEGGINING END THE END OF THE ROUTE
app.get('/login', function(req, res){
  res.render('login');
});

app.post('/login', passport.authenticate('local', {successRedirect: '/', failureRedirect:'/login'}), function(req, res){
});


//LOGOUT
app.get('/logout', function(req, res){
  req.logout();
  res.redirect('/login');
});

function isLoggedIn(req, res, next){
  if(req.isAuthenticated())
  {
    return next();
  }
  res.redirect('/login');
}
//Server configuration
app.get('*', function (req, res) {
  res.redirect('/login');
})

app.listen(process.env.PORT, function(){
  console.log('server running');
});
